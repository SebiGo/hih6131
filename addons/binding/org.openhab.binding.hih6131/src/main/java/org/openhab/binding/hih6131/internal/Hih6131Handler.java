/**
 * Copyright (c) 2010-2019 by the respective copyright holders.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.hih6131.internal;

import static org.openhab.binding.hih6131.internal.Hih6131BindingConstants.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.smarthome.core.library.types.DecimalType;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.ThingStatusDetail;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandler;
import org.eclipse.smarthome.core.types.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import com.pi4j.io.i2c.I2CFactory.UnsupportedBusNumberException;

/**
 * The {@link Hih6131Handler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author Sebastian Goodrick - Initial contribution
 */
@NonNullByDefault
public class Hih6131Handler extends BaseThingHandler {

    private final Logger logger = LoggerFactory.getLogger(Hih6131Handler.class);

    @Nullable
    private Hih6131Configuration config;

    public Hih6131Handler(Thing thing) {
        super(thing);
    }

    private Hih6131Dto convert(byte[] result, int time) throws IOException {
        Hih6131Dto ret = new Hih6131Dto();
        ret.setStatus((result[0] & 0xc0) >> 6);
        Float humidity = ((float) ((((result[0] & 0x3f) << 8) + result[1]) * 100)) / 16382;
        Float temperature = ((float) ((((result[2] << 8) + result[3]) >> 2) * 165)) / 16382 - 40;
        ret.setHumidity((float) Math.floor(humidity * 10) / 10);
        ret.setTemperature((float) Math.floor(temperature * 10) / 10);
        ret.setTime(time);
        return ret;
    }

    private Hih6131Dto readSensor(byte address)
            throws UnsupportedBusNumberException, IOException, InterruptedException {
        I2CBus i2cBus;
        i2cBus = I2CFactory.getInstance(I2CBus.BUS_1);
        I2CDevice hih6131 = i2cBus.getDevice(0x27);
        hih6131.write(address);
        byte[] result = new byte[4];
        for (int i = 0; i < 250; i++) {
            Thread.sleep(2);
            hih6131.read(address + 0x40, result, 0, 4);
            if ((result[0] & 0xc0) >> 6 == 0) {
                return convert(result, i * 2);
            }
        }
        logger.warn("HIH6131 did not change status to OK within 500ms, returning stale data.");
        return convert(result, 500);
    }

    private void updateAllChannels(Hih6131Dto hih6131) {
        updateState(CHANNEL_HUMIDITY, new DecimalType(hih6131.getHumidity()));
        updateState(CHANNEL_TEMPERATURE, new DecimalType(hih6131.getTemperature()));
        updateState(CHANNEL_STATUS, new DecimalType(hih6131.getStatus()));
    }

    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {

        try {
            Hih6131Dto hih6131 = readSensor((byte) 0x27);
            logger.debug("handleCommand() | Channel: " + channelUID.getId() + ", Command: " + command.toString());

            switch (channelUID.getId()) {
                case CHANNEL_HUMIDITY:
                    updateState(channelUID, new DecimalType(hih6131.getHumidity()));
                    break;
                case CHANNEL_TEMPERATURE:
                    updateState(channelUID, new DecimalType(hih6131.getTemperature()));
                    break;
                case CHANNEL_STATUS:
                    updateState(channelUID, new DecimalType(hih6131.getStatus()));
                    break;
                default:
                    updateStatus(ThingStatus.UNKNOWN, ThingStatusDetail.COMMUNICATION_ERROR,
                            "Could not get result from HIH6131, unknown channel '" + channelUID.getId() + "'");
                    break;
            }
        } catch (UnsupportedBusNumberException | IOException | InterruptedException e) {
            logger.warn("Exception ocurred: " + e.getMessage());
        }

    }

    @Override
    public void initialize() {
        logger.debug("Start initializing!");
        config = getConfigAs(Hih6131Configuration.class);

        updateStatus(ThingStatus.UNKNOWN);

        // try once and if successful, set sensor to online
        scheduler.execute(() -> {
            try {
                readSensor(config.address.byteValue());
                updateStatus(ThingStatus.ONLINE);
            } catch (UnsupportedBusNumberException | IOException | InterruptedException e) {
                updateStatus(ThingStatus.UNKNOWN, ThingStatusDetail.COMMUNICATION_ERROR, e.getMessage());
            }
        });

        // schedule every x seconds
        scheduler.scheduleWithFixedDelay(() -> {
            try {
                updateAllChannels(readSensor(config.address.byteValue()));
            } catch (UnsupportedBusNumberException | IOException | InterruptedException e) {
                updateStatus(ThingStatus.UNKNOWN, ThingStatusDetail.COMMUNICATION_ERROR, e.getMessage());
            }
        }, config.interval.longValue(), config.interval.longValue(), TimeUnit.SECONDS);

        logger.debug("Finished initializing!");
    }
}
